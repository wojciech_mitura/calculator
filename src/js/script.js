window.addEventListener('load',function() {
    
    let display = document.getElementById("display"),
        contentButtons = document.getElementsByClassName("content-buttons")[0];


    function blinkTextOnError() {
        display.classList.toggle("blink-blink");
        setTimeout(function() {
            display.classList.toggle("blink-blink");
        }, 500)
    }

    contentButtons.addEventListener("click", function(event) {
        let calculatorButton = event.target;
        let displayLastNumber = (display.innerText).match(/[\d.]*$/)[0];

        function isDisplayHasOpertorOnLastPosition() {
            let displayLastChar = display.innerText.slice(-1);
            let operators = ['+', '-', '*', '/', '.'];

            return operators.includes(displayLastChar);
        }

        /**
         * jeśli klikniesz w content-buttons(nie trafisz w button) 
         * to wychodzisz z funkcji.
         */
        if (calculatorButton.classList.contains("content-buttons")) {
            return;
        }

        /**
         * po kliknięciu "ac" czyści wszystko z wyświetlacza do 0 
         */
        if (calculatorButton.classList.contains("clear-all")) {
            display.innerText = "0";
            return;
        }

        /**
         * nie pozwala na dublowanie operatorów
         */
        if (
            isDisplayHasOpertorOnLastPosition() &&
            calculatorButton.classList.contains('operator')
        ) {
            return;
        }

        /**
         * usuwa ostatni znak z wyświetlacza 
         * i podstawia "0" jeśli chcemy usunąć jednocyftową liczbę
         */
        if (calculatorButton.classList.contains("clear-last-char")) {
            if (display.innerText.length === 1) {
                display.innerText = "0";
                return
            }
            display.innerText = display.innerText.slice(0, -1); 
            return;   
        } 

        /** 
         * Jeśli ostatnim znakiem na wyświetlaczu jest "operator" to zamiast "." wstawia "0."
         */
        if (
            calculatorButton.classList.contains("dot") && 
            isDisplayHasOpertorOnLastPosition()
        ) {
            display.innerText +="0";
        }

        
        /**
         * jeśli na wyświetlaczu jeste tylko "0" to po wciśnięciu "."
         * wstawia "0."
         * 
         * po naciśnięciu "." po "(" wstawia "0."
         */
        if (
            calculatorButton.innerText === "." && 
            (display.innerText.length === 1) &&
            display.innerText.slice(-1) !== "("
        ) {
            display.innerText += ".";
            return     
        } else if (
            (calculatorButton.innerText === "." && display.innerText.slice(-1) == "(") ||
            (calculatorButton.innerText === "." && display.innerText.slice(-1) == ")")
        ) {
            display.innerText += "0";
        }

        /**
         * jeśli na wyświetlaczu jest tylko "0" lub "(" to po wciśnięciu "operatora" ( wykluczając "-") wychodzi z funkcji 
         */
        if (
            display.innerText.length === 1 &&
            calculatorButton.classList.contains('operator') &&
            calculatorButton.innerText !== "-" &&
            (display.innerText.slice(-1) == "0" || display.innerText.slice(-1) == "(")
            ) {
                return
            }

        /**
         * po wciśnieciu "=" wyświetla wynik
         * przy errorze dodaje klase (zmienia kolor tekstu na czerwony)
         */
        try {
            if (calculatorButton.innerText === "=") { 
                display.innerText = stringMath(display.innerText)
                return;
            } 
        }
        catch (error) {
            blinkTextOnError()

            return;
        }

        /**
         * blokowanie dodawania kolejnej kropki dla ostatniej 
         * liczby w wyswietlaczu
         */
        if (
            displayLastNumber.includes(".") &&
            calculatorButton.innerText == "."
        ) {
            return;
        } 

        /**
         * nawias zamykający/otwierający
         */
        if (calculatorButton.innerText == ")") {
            let frontBracketsQuantity = 0,
                backBracketsQuantity = 0;

            if (display.innerText.slice(-1) == "(") {
                blinkTextOnError();
                return;
            }

            for (var i = 0; i < display.innerText.length; ++i) {
                if (display.innerText[i] === "(") {
                    frontBracketsQuantity++;
                } 
                else if (display.innerText[i] === ")") {
                    backBracketsQuantity++;
                }
            }

            if (frontBracketsQuantity === backBracketsQuantity) {
                return;
            }
        }

        /**
         * niemożna dodawać kolejnych "0"
         * jeśli liczba jest "0"
         */
        if (display.innerText == "0") {
            display.innerText = ""
        } else if (
            displayLastNumber == "0" && 
            Number.isInteger(parseInt(calculatorButton.innerText)) &&
            calculatorButton.innerText == "0"
        ) {
            blinkTextOnError();
            return
        }
    
        /**
         * dodaje liczbę z przycisku do wyświetlacza
         */
        display.innerText += calculatorButton.innerText;
    })
})
